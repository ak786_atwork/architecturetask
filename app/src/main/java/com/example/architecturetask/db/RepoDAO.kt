package com.example.architecturetask.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.architecturetask.model.Repo

@Dao
interface RepoDAO {
    @Query("SELECT * FROM repo ")
    suspend fun fetchMyRepos(): List<Repo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveMyRepos(repos: List<Repo>)
}
