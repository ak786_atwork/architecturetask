package com.example.architecturetask.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.architecturetask.model.Repo


@Database(entities = [Repo::class], version = 1)
abstract class RepoDatabase : RoomDatabase() {
    abstract val repoDAO: RepoDAO

    companion object {
        var database: RepoDatabase? = null
        fun getInstance(context: Context?): RepoDatabase {

            if (database == null) {
//                synchronized(this) {
                if (database == null) {
                    database = Room.databaseBuilder(
                        context!!,
                        RepoDatabase::class.java, "repo.db"
                    ).fallbackToDestructiveMigration()
                        .build()
                }
            }

//            }
            return database!!
        }
    }
}
