package com.example.architecturetask.model

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = "repo")
data class Repo(
    @PrimaryKey
    @NonNull
    @Json(name = "id")
    val id : String,

    @ColumnInfo(name = "name")
    @Json(name = "name")
    val repoName : String,

    @ColumnInfo(name = "description")
    @Json(name = "description")
    @Nullable
    val description : String?,

    @ColumnInfo(name = "language")
    @Json(name = "language")
    @Nullable
    val language : String?,

    @ColumnInfo(name = "starCount")
    @Json(name = "stargazers_count")
    val starCount: Int
)

