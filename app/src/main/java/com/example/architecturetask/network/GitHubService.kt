package com.example.architecturetask.network

import androidx.lifecycle.LiveData
import com.example.architecturetask.model.Repo
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubService {
    @GET("users/{user}/starred")
    suspend fun getStarredRepos(@Path("user") username: String): List<Repo>
}
