package com.example.architecturetask.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

//Singleton class
object GithubApiClient {
    private var httpLoggingInterceptor: HttpLoggingInterceptor? = null
    private const val BASE_URL = "https://api.github.com"
    private var retrofit: Retrofit? = null
    private var gitHubService: GitHubService? = null


    fun makeRetrofitService(): GitHubService {
        httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor!!.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor!!)
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
            .build().create(GitHubService::class.java)
    }

}