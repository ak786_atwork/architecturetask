package com.example.architecturetask.repository

import androidx.lifecycle.LiveData
import com.example.architecturetask.model.Repo

interface RepoDataSourceInterface {
    suspend fun fetchRepos(username: String): List<Repo>
    suspend fun saveRepos(repos: List<Repo>)
}
