package com.example.architecturetask.repository

import androidx.lifecycle.LiveData
import com.example.architecturetask.MyApplication
import com.example.architecturetask.db.RepoDatabase
import com.example.architecturetask.model.Repo

class RepoLocalSource : RepoDataSourceInterface {
    override suspend fun fetchRepos(username: String): List<Repo> {
       return RepoDatabase.getInstance(MyApplication.context).repoDAO.fetchMyRepos()
    }

    override suspend fun saveRepos(repos: List<Repo>) {
        RepoDatabase.getInstance(MyApplication.context).repoDAO.saveMyRepos(repos)
    }
}
