package com.example.architecturetask.repository

import androidx.lifecycle.LiveData
import com.example.architecturetask.model.Repo
import com.example.architecturetask.network.GithubApiClient

class RepoRemoteSource : RepoDataSourceInterface {

    override suspend fun fetchRepos(username: String): List<Repo> {
        return GithubApiClient.makeRetrofitService().getStarredRepos(username)
    }

    override suspend fun saveRepos(repos: List<Repo>) {
    }
}
