package com.example.architecturetask.repository

import android.content.Context
import android.net.ConnectivityManager
import com.example.architecturetask.MyApplication
import com.example.architecturetask.model.Repo

class RepoRepository(
    private val repoLocalSource: RepoLocalSource,
    private val repoRemoteSource: RepoRemoteSource
) :
    RepoDataSourceInterface {

    override suspend fun saveRepos(repos: List<Repo>) {
        repoLocalSource.saveRepos(repos)
    }

    override suspend fun fetchRepos(username: String): List<Repo> {
        //rxjava sample
        /* return Observable.concatArray(
           repoLocalSource.fetchRepos(username),
           repoRemoteSource.fetchRepos(username)
       )
           .doOnNext({ repos -> saveRepos(repos) })
           .onErrorResumeNext(Observable.empty())*/
        //merge both resources from local and remote, show cache if network not available
        //just for demo

        var repos: List<Repo>
        if (isOnline()) {
            repos = repoRemoteSource.fetchRepos(username)
            saveRepos(repos)
        } else {
            repos = repoLocalSource.fetchRepos(username)
        }
        return repos
    }


    private fun isOnline(): Boolean {
        val cm =
            MyApplication.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val netInfo = cm!!.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

}