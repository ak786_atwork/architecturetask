package com.example.architecturetask.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.architecturetask.model.Repo
import com.example.architecturetask.repository.RepoLocalSource
import com.example.architecturetask.repository.RepoRemoteSource
import com.example.architecturetask.repository.RepoRepository
import kotlinx.coroutines.launch


class RepoViewModel : ViewModel() {

    private val repoLiveData: MutableLiveData<List<Repo>> =
        MutableLiveData<List<Repo>>()
    private val repoRepository: RepoRepository =
        RepoRepository(RepoLocalSource(), RepoRemoteSource())

    fun getMyStarredList(username: String) {
        viewModelScope.launch {
            repoLiveData.postValue(repoRepository.fetchRepos(username))
        }
    }

    fun getRepoLiveData(): MutableLiveData<List<Repo>> {
        return repoLiveData
    }

}