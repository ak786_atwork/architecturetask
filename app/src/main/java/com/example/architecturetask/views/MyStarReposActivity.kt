package com.example.architecturetask.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.architecturetask.R
import com.example.architecturetask.model.Repo
import com.example.architecturetask.viewmodel.RepoViewModel
import com.example.architecturetask.views.adapter.GitHubRepoAdapter
import java.util.*

class MyStarReposActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var repoArrayList: List<Repo>
    private lateinit var gitHubRepoAdapter: GitHubRepoAdapter
    private lateinit var viewModel: RepoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_star_repos)

        repoArrayList = ArrayList<Repo>()

        val linearLayoutManager = LinearLayoutManager(this)
        val dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)

        gitHubRepoAdapter = GitHubRepoAdapter()
        gitHubRepoAdapter.setReposList(repoArrayList)
        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.apply {
            addItemDecoration(dividerItemDecoration)
            layoutManager = linearLayoutManager
            adapter = gitHubRepoAdapter
        }

        viewModel = ViewModelProviders.of(this).get<RepoViewModel>(RepoViewModel::class.java)
        getStarredRepos(viewModel)
        observeMyStars(viewModel)
    }

    private fun observeMyStars(viewModel: RepoViewModel) {
        viewModel
            .getRepoLiveData()
            .observe(this, androidx.lifecycle.Observer { gitHubRepoAdapter.setReposList(it)})

    }

    private fun getStarredRepos(viewModel: RepoViewModel) {
        viewModel.getMyStarredList("anil")
    }

}